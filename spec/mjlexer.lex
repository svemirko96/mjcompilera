package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{

	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type) {
		return new Symbol(type, yyline + 1, yycolumn);
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn, value);
	}

%}

%cup
%line
%column

%xstate COMMENT

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

" "		{ }
"\b"	{ }
"\t"	{ }
"\r\n"	{ }
"\f"	{ }

"program"	{ return new_symbol(sym.PROGRAM, yytext()); }
"const"		{ return new_symbol(sym.CONST, yytext()); }
"new"		{ return new_symbol(sym.NEW, yytext()); }
"print"		{ return new_symbol(sym.PRINT, yytext()); }
"read"		{ return new_symbol(sym.READ, yytext()); }
"return"	{ return new_symbol(sym.RETURN, yytext()); }
"void"		{ return new_symbol(sym.VOID, yytext()); }
"subsetof"	{ return new_symbol(sym.SUBSETOF, yytext()); }
"goto"		{ return new_symbol(sym.GOTO, yytext()); }

"+"			{ return new_symbol(sym.PLUS, yytext()); }
"-"			{ return new_symbol(sym.MINUS, yytext()); }
"*"			{ return new_symbol(sym.ASTERISK, yytext()); }
"/"			{ return new_symbol(sym.FSLASH, yytext()); }
"%"			{ return new_symbol(sym.MODULO, yytext()); }
"="			{ return new_symbol(sym.ASSIGN, yytext()); }
"+="		{ return new_symbol(sym.ADDEQ, yytext()); }
"-="		{ return new_symbol(sym.SUBEQ, yytext()); }
"*="		{ return new_symbol(sym.MULEQ, yytext()); }
"/="		{ return new_symbol(sym.DIVEQ, yytext()); }
"%="		{ return new_symbol(sym.MODEQ, yytext()); }
"++"		{ return new_symbol(sym.INC, yytext()); }
"--"		{ return new_symbol(sym.DEC, yytext()); }
";"			{ return new_symbol(sym.SEMICOL, yytext()); }
":"			{ return new_symbol(sym.COLON, yytext()); }
","			{ return new_symbol(sym.COMMA, yytext()); }
"("			{ return new_symbol(sym.LPAREN, yytext()); }
")"			{ return new_symbol(sym.RPAREN, yytext()); }
"["			{ return new_symbol(sym.LBRACKET, yytext()); }
"]"			{ return new_symbol(sym.RBRACKET, yytext()); }
"{"			{ return new_symbol(sym.LBRACE, yytext()); }
"}"			{ return new_symbol(sym.RBRACE, yytext()); }

"//"				{ yybegin(COMMENT); }
<COMMENT> . 		{ yybegin(COMMENT); }
<COMMENT> "\r\n"	{ yybegin(YYINITIAL); }

[0-9]+				{ return new_symbol(sym.NUM_CONST, new Integer(yytext())); }
'[ -~]'				{ return new_symbol(sym.CHAR_CONST, new Character(yytext().charAt(1))); }
(true|false)		{ return new_symbol(sym.BOOL_CONST, new Integer(yytext().equals("true") ? 1 : 0)); }

([a-z]|[A-Z])[a-z|A-Z|0-9|_]* { return new_symbol(sym.IDENT, yytext()); }

. { System.err.println("Leksicka greska (" + yytext() + ") na liniji " + (yyline + 1)); }