Project for a university course about compilers.

Compiler is made for a Java-like minimalist language.
It parses given source file and makes abstract syntax tree (AST) out of it, with the possibility of error recovery due to a syntax error.
AST is later used for semantic check and bytecode generation using utility class Code, provided in library.
The compiled bytecode can be then executed using Run class which acts as a very basic JVM-like execution environment.

Lexer used is JLex while the parser is modified CUP parser which generates AST given some aditional info.