package rs.ac.bg.etf.pp1;

import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class SymTab extends Tab {

	// prosirenje standardnih tipova klase Tab
	public static Struct boolType = new Struct(Struct.Bool);
	
	public static Obj printObj, readObj;
	
	public static void init() {
		
		Tab.init();
		
		currentScope().addToLocals(new Obj(Obj.Type, "bool", boolType, 0, 1));
		
	}
	
}
