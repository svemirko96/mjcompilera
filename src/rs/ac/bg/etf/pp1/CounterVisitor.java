package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.FormalParamArray;
import rs.ac.bg.etf.pp1.ast.FormalParamScalar;
import rs.ac.bg.etf.pp1.ast.MultipleActualParams;
import rs.ac.bg.etf.pp1.ast.SingleActualParam;
import rs.ac.bg.etf.pp1.ast.SingleVarDeclaration;
import rs.ac.bg.etf.pp1.ast.VarDeclarationlList;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;

public class CounterVisitor extends VisitorAdaptor {

	protected int count;
	
	public int getCount() {
		return count;
	}
	
	public static class FormalParamCounter extends CounterVisitor {
		
		public void visit(FormalParamScalar formalParamScalar) {
			count++;
		}
		
		public void visit(FormalParamArray formalParamArray) {
			count++;
		}
		
	}
	
	// broji lokalne promenljive unutar funkcija
	public static class VarCounter extends CounterVisitor {
		
		public void visit(VarDeclarationlList varDeclarationlList) {
			count++;
		}
		
		public void visit(SingleVarDeclaration singleVarDeclaration) {
			count++;
		}
		
	}
	
	public static class ActualParamCounter extends CounterVisitor {
		
		public void visit(MultipleActualParams multipleActualParams) {
			count ++;
		}
		
		public void visit(SingleActualParam singleActualParam) {
			count++;
		}
		
	}
	
}
