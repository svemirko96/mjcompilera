// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public class ProgramMembersList extends ProgMemberList {

    private ProgMemberList ProgMemberList;
    private ProgMember ProgMember;

    public ProgramMembersList (ProgMemberList ProgMemberList, ProgMember ProgMember) {
        this.ProgMemberList=ProgMemberList;
        if(ProgMemberList!=null) ProgMemberList.setParent(this);
        this.ProgMember=ProgMember;
        if(ProgMember!=null) ProgMember.setParent(this);
    }

    public ProgMemberList getProgMemberList() {
        return ProgMemberList;
    }

    public void setProgMemberList(ProgMemberList ProgMemberList) {
        this.ProgMemberList=ProgMemberList;
    }

    public ProgMember getProgMember() {
        return ProgMember;
    }

    public void setProgMember(ProgMember ProgMember) {
        this.ProgMember=ProgMember;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ProgMemberList!=null) ProgMemberList.accept(visitor);
        if(ProgMember!=null) ProgMember.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ProgMemberList!=null) ProgMemberList.traverseTopDown(visitor);
        if(ProgMember!=null) ProgMember.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ProgMemberList!=null) ProgMemberList.traverseBottomUp(visitor);
        if(ProgMember!=null) ProgMember.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ProgramMembersList(\n");

        if(ProgMemberList!=null)
            buffer.append(ProgMemberList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ProgMember!=null)
            buffer.append(ProgMember.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ProgramMembersList]");
        return buffer.toString();
    }
}
