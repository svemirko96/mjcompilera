// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public class Program implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private ProgName ProgName;
    private ProgMemberList ProgMemberList;
    private MethodsDeclList MethodsDeclList;

    public Program (ProgName ProgName, ProgMemberList ProgMemberList, MethodsDeclList MethodsDeclList) {
        this.ProgName=ProgName;
        if(ProgName!=null) ProgName.setParent(this);
        this.ProgMemberList=ProgMemberList;
        if(ProgMemberList!=null) ProgMemberList.setParent(this);
        this.MethodsDeclList=MethodsDeclList;
        if(MethodsDeclList!=null) MethodsDeclList.setParent(this);
    }

    public ProgName getProgName() {
        return ProgName;
    }

    public void setProgName(ProgName ProgName) {
        this.ProgName=ProgName;
    }

    public ProgMemberList getProgMemberList() {
        return ProgMemberList;
    }

    public void setProgMemberList(ProgMemberList ProgMemberList) {
        this.ProgMemberList=ProgMemberList;
    }

    public MethodsDeclList getMethodsDeclList() {
        return MethodsDeclList;
    }

    public void setMethodsDeclList(MethodsDeclList MethodsDeclList) {
        this.MethodsDeclList=MethodsDeclList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ProgName!=null) ProgName.accept(visitor);
        if(ProgMemberList!=null) ProgMemberList.accept(visitor);
        if(MethodsDeclList!=null) MethodsDeclList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ProgName!=null) ProgName.traverseTopDown(visitor);
        if(ProgMemberList!=null) ProgMemberList.traverseTopDown(visitor);
        if(MethodsDeclList!=null) MethodsDeclList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ProgName!=null) ProgName.traverseBottomUp(visitor);
        if(ProgMemberList!=null) ProgMemberList.traverseBottomUp(visitor);
        if(MethodsDeclList!=null) MethodsDeclList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("Program(\n");

        if(ProgName!=null)
            buffer.append(ProgName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ProgMemberList!=null)
            buffer.append(ProgMemberList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodsDeclList!=null)
            buffer.append(MethodsDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [Program]");
        return buffer.toString();
    }
}
