// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(ReturnType ReturnType) { }
    public void visit(Mulop Mulop) { }
    public void visit(ProgMemberList ProgMemberList) { }
    public void visit(Assignop Assignop) { }
    public void visit(FormalParamDecl FormalParamDecl) { }
    public void visit(StatementList StatementList) { }
    public void visit(Addop Addop) { }
    public void visit(Factor Factor) { }
    public void visit(AddopLeft AddopLeft) { }
    public void visit(Designator Designator) { }
    public void visit(Term Term) { }
    public void visit(ProgMember ProgMember) { }
    public void visit(ConstValue ConstValue) { }
    public void visit(ActualParams ActualParams) { }
    public void visit(AddopRight AddopRight) { }
    public void visit(ActualParamList ActualParamList) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(FormalParamList FormalParamList) { }
    public void visit(Expr Expr) { }
    public void visit(FormalParams FormalParams) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(VarDecls VarDecls) { }
    public void visit(MethodsDeclList MethodsDeclList) { }
    public void visit(ConstAssignList ConstAssignList) { }
    public void visit(Statement Statement) { }
    public void visit(MulopLeft MulopLeft) { }
    public void visit(MulopRight MulopRight) { }
    public void visit(SingleVarDecl SingleVarDecl) { }
    public void visit(ModEq ModEq) { visit(); }
    public void visit(DivEq DivEq) { visit(); }
    public void visit(MulEq MulEq) { visit(); }
    public void visit(Mod Mod) { visit(); }
    public void visit(Div Div) { visit(); }
    public void visit(Mul Mul) { visit(); }
    public void visit(MulopR MulopR) { visit(); }
    public void visit(MulopL MulopL) { visit(); }
    public void visit(SubEq SubEq) { visit(); }
    public void visit(AddEq AddEq) { visit(); }
    public void visit(Sub Sub) { visit(); }
    public void visit(Add Add) { visit(); }
    public void visit(AddopR AddopR) { visit(); }
    public void visit(AddopL AddopL) { visit(); }
    public void visit(AssignMul AssignMul) { visit(); }
    public void visit(AssignAdd AssignAdd) { visit(); }
    public void visit(AssignBasic AssignBasic) { visit(); }
    public void visit(ArrayDesignatorId ArrayDesignatorId) { visit(); }
    public void visit(DesignatorScalar DesignatorScalar) { visit(); }
    public void visit(DesignatorArrayEl DesignatorArrayEl) { visit(); }
    public void visit(FuncCallFactor FuncCallFactor) { visit(); }
    public void visit(NegatedFactor NegatedFactor) { visit(); }
    public void visit(NewArrayFactor NewArrayFactor) { visit(); }
    public void visit(GroupedExprFactor GroupedExprFactor) { visit(); }
    public void visit(ConstFactor ConstFactor) { visit(); }
    public void visit(DesignatorFactor DesignatorFactor) { visit(); }
    public void visit(SingleFactor SingleFactor) { visit(); }
    public void visit(MultipleFactors MultipleFactors) { visit(); }
    public void visit(SubsetOperand SubsetOperand) { visit(); }
    public void visit(SubsetOfExpr SubsetOfExpr) { visit(); }
    public void visit(SimpleExpr SimpleExpr) { visit(); }
    public void visit(ComplexExpr ComplexExpr) { visit(); }
    public void visit(DecrementStatement DecrementStatement) { visit(); }
    public void visit(IncrementStatement IncrementStatement) { visit(); }
    public void visit(AssignmentStatement AssignmentStatement) { visit(); }
    public void visit(SingleActualParam SingleActualParam) { visit(); }
    public void visit(MultipleActualParams MultipleActualParams) { visit(); }
    public void visit(NoActualParams NoActualParams) { visit(); }
    public void visit(ActualParamsList ActualParamsList) { visit(); }
    public void visit(GotoStatement GotoStatement) { visit(); }
    public void visit(LabelDeclStatement LabelDeclStatement) { visit(); }
    public void visit(FuncCallStatement FuncCallStatement) { visit(); }
    public void visit(PrintSpecWidthStatement PrintSpecWidthStatement) { visit(); }
    public void visit(PrintNoWidthStatement PrintNoWidthStatement) { visit(); }
    public void visit(ReadStatement ReadStatement) { visit(); }
    public void visit(ReturnExprStatement ReturnExprStatement) { visit(); }
    public void visit(ReturnStatement ReturnStatement) { visit(); }
    public void visit(ErrorStatement ErrorStatement) { visit(); }
    public void visit(DesignatorStmt DesignatorStmt) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(EmptyStatementList EmptyStatementList) { visit(); }
    public void visit(StatementsList StatementsList) { visit(); }
    public void visit(FormalParamArray FormalParamArray) { visit(); }
    public void visit(FormalParamScalar FormalParamScalar) { visit(); }
    public void visit(SingleFormalParam SingleFormalParam) { visit(); }
    public void visit(MultipleFormalParams MultipleFormalParams) { visit(); }
    public void visit(NoFormalParams NoFormalParams) { visit(); }
    public void visit(FormalParamsList FormalParamsList) { visit(); }
    public void visit(ReturnTypeVoid ReturnTypeVoid) { visit(); }
    public void visit(ReturnTypeScalar ReturnTypeScalar) { visit(); }
    public void visit(MethodTypeName MethodTypeName) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(EmptyMethodsDeclList EmptyMethodsDeclList) { visit(); }
    public void visit(MethodsDeclsList MethodsDeclsList) { visit(); }
    public void visit(NoVarDeclaration NoVarDeclaration) { visit(); }
    public void visit(VarDeclarations VarDeclarations) { visit(); }
    public void visit(InvalidVarDecl InvalidVarDecl) { visit(); }
    public void visit(VarDeclScalar VarDeclScalar) { visit(); }
    public void visit(VarDeclArray VarDeclArray) { visit(); }
    public void visit(SingleVarDeclaration SingleVarDeclaration) { visit(); }
    public void visit(VarDeclarationlList VarDeclarationlList) { visit(); }
    public void visit(VarDeclType VarDeclType) { visit(); }
    public void visit(VarDecl VarDecl) { visit(); }
    public void visit(BoolConst BoolConst) { visit(); }
    public void visit(CharConst CharConst) { visit(); }
    public void visit(NumConst NumConst) { visit(); }
    public void visit(ConstAssignment ConstAssignment) { visit(); }
    public void visit(SingleConstAssignment SingleConstAssignment) { visit(); }
    public void visit(ConstAssignmentList ConstAssignmentList) { visit(); }
    public void visit(ConstDeclType ConstDeclType) { visit(); }
    public void visit(ConstDecl ConstDecl) { visit(); }
    public void visit(VarProgMember VarProgMember) { visit(); }
    public void visit(ConstProgMember ConstProgMember) { visit(); }
    public void visit(EmptyProgramMembersList EmptyProgramMembersList) { visit(); }
    public void visit(ProgramMembersList ProgramMembersList) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
