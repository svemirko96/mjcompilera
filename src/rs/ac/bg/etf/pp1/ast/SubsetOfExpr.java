// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public class SubsetOfExpr extends Expr {

    private SubsetOperand SubsetOperand;
    private SubsetOperand SubsetOperand1;

    public SubsetOfExpr (SubsetOperand SubsetOperand, SubsetOperand SubsetOperand1) {
        this.SubsetOperand=SubsetOperand;
        if(SubsetOperand!=null) SubsetOperand.setParent(this);
        this.SubsetOperand1=SubsetOperand1;
        if(SubsetOperand1!=null) SubsetOperand1.setParent(this);
    }

    public SubsetOperand getSubsetOperand() {
        return SubsetOperand;
    }

    public void setSubsetOperand(SubsetOperand SubsetOperand) {
        this.SubsetOperand=SubsetOperand;
    }

    public SubsetOperand getSubsetOperand1() {
        return SubsetOperand1;
    }

    public void setSubsetOperand1(SubsetOperand SubsetOperand1) {
        this.SubsetOperand1=SubsetOperand1;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(SubsetOperand!=null) SubsetOperand.accept(visitor);
        if(SubsetOperand1!=null) SubsetOperand1.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(SubsetOperand!=null) SubsetOperand.traverseTopDown(visitor);
        if(SubsetOperand1!=null) SubsetOperand1.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(SubsetOperand!=null) SubsetOperand.traverseBottomUp(visitor);
        if(SubsetOperand1!=null) SubsetOperand1.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SubsetOfExpr(\n");

        if(SubsetOperand!=null)
            buffer.append(SubsetOperand.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(SubsetOperand1!=null)
            buffer.append(SubsetOperand1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SubsetOfExpr]");
        return buffer.toString();
    }
}
