// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public class MethodsDeclsList extends MethodsDeclList {

    private MethodsDeclList MethodsDeclList;
    private MethodDecl MethodDecl;

    public MethodsDeclsList (MethodsDeclList MethodsDeclList, MethodDecl MethodDecl) {
        this.MethodsDeclList=MethodsDeclList;
        if(MethodsDeclList!=null) MethodsDeclList.setParent(this);
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
    }

    public MethodsDeclList getMethodsDeclList() {
        return MethodsDeclList;
    }

    public void setMethodsDeclList(MethodsDeclList MethodsDeclList) {
        this.MethodsDeclList=MethodsDeclList;
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodsDeclList!=null) MethodsDeclList.accept(visitor);
        if(MethodDecl!=null) MethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodsDeclList!=null) MethodsDeclList.traverseTopDown(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodsDeclList!=null) MethodsDeclList.traverseBottomUp(visitor);
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodsDeclsList(\n");

        if(MethodsDeclList!=null)
            buffer.append(MethodsDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodsDeclsList]");
        return buffer.toString();
    }
}
