// generated with ast extension for cup
// version 0.8
// 20/7/2020 0:33:55


package rs.ac.bg.etf.pp1.ast;

public class SubsetOperand implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private ArrayDesignatorId ArrayDesignatorId;

    public SubsetOperand (ArrayDesignatorId ArrayDesignatorId) {
        this.ArrayDesignatorId=ArrayDesignatorId;
        if(ArrayDesignatorId!=null) ArrayDesignatorId.setParent(this);
    }

    public ArrayDesignatorId getArrayDesignatorId() {
        return ArrayDesignatorId;
    }

    public void setArrayDesignatorId(ArrayDesignatorId ArrayDesignatorId) {
        this.ArrayDesignatorId=ArrayDesignatorId;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ArrayDesignatorId!=null) ArrayDesignatorId.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ArrayDesignatorId!=null) ArrayDesignatorId.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ArrayDesignatorId!=null) ArrayDesignatorId.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SubsetOperand(\n");

        if(ArrayDesignatorId!=null)
            buffer.append(ArrayDesignatorId.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SubsetOperand]");
        return buffer.toString();
    }
}
