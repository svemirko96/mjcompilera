package rs.ac.bg.etf.pp1;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rs.ac.bg.etf.pp1.CounterVisitor.FormalParamCounter;
import rs.ac.bg.etf.pp1.CounterVisitor.VarCounter;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;

public class CodeGenerator extends VisitorAdaptor {

	private int mainPc;
	
	// goto strukture
	private static class ForwardReference {
		String label;
		int location;
		
		public ForwardReference(String lab, int loc) {
			label = lab;
			location = loc;
		}
	}
	Map<String, Integer> labelLocations = new HashMap<String, Integer>();
	List<ForwardReference> forwardRefs = new ArrayList<>();	
	
	public int getMainPc() {
		return mainPc;
	}
	
	public CodeGenerator() {
		
		// ugradjivanje koda na pocetak fajla za predefinisane funkcije
		
		// chr 
		Obj chrObj = SymTab.find("chr");
		chrObj.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.load(chrObj.getLocalSymbols().iterator().next());
		Code.put(Code.exit);
		Code.put(Code.return_);
		
		// ord 
		Obj ordObj = SymTab.find("ord");
		ordObj.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.load(ordObj.getLocalSymbols().iterator().next());
		Code.put(Code.exit);
		Code.put(Code.return_);
		
		// len 
		Obj lenObj = SymTab.find("len");
		lenObj.setAdr(Code.pc);
		Code.put(Code.enter);
		Code.put(1);
		Code.put(1);
		Code.load(lenObj.getLocalSymbols().iterator().next());
		Code.put(Code.arraylength);
		Code.put(Code.exit);
		Code.put(Code.return_);
		
	}
	
	public void visit(MethodDecl methodDecl) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void visit(MethodTypeName methodTypeName) {
		
		if(methodTypeName.getMethodId().equals("main")) {
			mainPc = Code.pc;
		}
		methodTypeName.obj.setAdr(Code.pc);
		
		SyntaxNode methodNode = methodTypeName.getParent();
		
		VarCounter varCnt = new VarCounter();
		methodNode.traverseTopDown(varCnt);
		
		FormalParamCounter fpCnt = new FormalParamCounter();
		methodNode.traverseTopDown(fpCnt);
		
		Code.put(Code.enter);
		Code.put(fpCnt.getCount());
		Code.put(fpCnt.getCount() + varCnt.getCount());
	}
	
	public void visit(ReturnStatement returnStatement) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void visit(ReturnExprStatement returnExprStatement) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void visit(ReadStatement readStatement) {
		
		if(readStatement.getDesignator() instanceof DesignatorScalar) {
			if(readStatement.getDesignator().obj.getType() == SymTab.charType)
				Code.put(Code.bread);
			else
				Code.put(Code.read);
			Code.store(readStatement.getDesignator().obj);
		} else { // DesignatorArrayEl
			if(readStatement.getDesignator().obj.getType().getElemType() == SymTab.charType)
				Code.put(Code.bread);
			else
				Code.put(Code.read);
			Code.put(Code.astore);
		}
		
	}
	
	public void visit(PrintNoWidthStatement printNoWidthStatement) {
		if(printNoWidthStatement.getExpr().struct != SymTab.charType) {
			Code.loadConst(0);
			Code.put(Code.print);
		} else {
			Code.loadConst(0);
			Code.put(Code.bprint);
		}
	}
	
	public void visit(PrintSpecWidthStatement printSpecWidthStatement) {
		if(printSpecWidthStatement.getExpr().struct != SymTab.charType) {
			Code.loadConst(printSpecWidthStatement.getN2());
			Code.put(Code.print);
		} else {
			Code.loadConst(printSpecWidthStatement.getN2());
			Code.put(Code.bprint);
		}
	}
	
	public void visit(FuncCallStatement funcCallStatement) {
		Obj functionObj = funcCallStatement.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		Code.put(Code.call);
		Code.put2(offset);
		
		// Ako funkcija vraca vrednost, skinuti je sa expr steka jer se ne cuva nigde
		if(funcCallStatement.getDesignator().obj.getType() != Tab.noType) {
			Code.put(Code.pop);
		}
	}
	
	public void visit(AssignmentStatement assignmentStatement) {
		
		if(assignmentStatement.getAssignop() instanceof AssignAdd) {
			AssignAdd addop = (AssignAdd)assignmentStatement.getAssignop();
			if(addop.getAddopRight() instanceof AddEq) { Code.put(Code.add); }
			else if(addop.getAddopRight() instanceof SubEq) { Code.put(Code.sub); }
		}
		else if(assignmentStatement.getAssignop() instanceof AssignMul) {
			AssignMul mulop = (AssignMul)assignmentStatement.getAssignop();
			if(mulop.getMulopRight() instanceof MulEq) { Code.put(Code.mul); }
			else if(mulop.getMulopRight() instanceof DivEq) { Code.put(Code.div); }
			else { Code.put(Code.rem); }
		}
		
		if(assignmentStatement.getDesignator() instanceof DesignatorArrayEl) {

			if(assignmentStatement.getDesignator().obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.bastore);
			} else {
				Code.put(Code.astore);
			}
			
		} else { // DesignatorScalar
			Code.store(assignmentStatement.getDesignator().obj);
		}
	}
	
	public void visit(IncrementStatement incrementStatement) {
		
		if(incrementStatement.getDesignator() instanceof DesignatorArrayEl) {

			Code.put(Code.dup2);
			if(incrementStatement.getDesignator().obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.baload);
			} else {
				Code.put(Code.aload);
			}
			Code.loadConst(1);
			Code.put(Code.add);
			if(incrementStatement.getDesignator().obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.bastore);
			} else {
				Code.put(Code.astore);
			}
			
		} else { // DesignatorScalar
			Code.load(incrementStatement.getDesignator().obj);
			Code.loadConst(1);
			Code.put(Code.add);
			Code.store(incrementStatement.getDesignator().obj);
		}
	}
	
	public void visit(DecrementStatement decrementStatement) {
		
		if(decrementStatement.getDesignator() instanceof DesignatorArrayEl) {

			Code.put(Code.dup2);
			if(decrementStatement.getDesignator().obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.baload);
			} else {
				Code.put(Code.aload);
			}
			Code.loadConst(1);
			Code.put(Code.sub);
			if(decrementStatement.getDesignator().obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.bastore);
			} else {
				Code.put(Code.astore);
			}
			
		} else { // DesignatorScalar
			Code.load(decrementStatement.getDesignator().obj);
			Code.loadConst(1);
			Code.put(Code.sub);
			Code.store(decrementStatement.getDesignator().obj);
		}
	}
	
	public void visit(LabelDeclStatement labelDeclStatement) {
		
		String label = labelDeclStatement.getLabel();
		labelLocations.put(label, Code.pc);
		
		for(ForwardReference fr : forwardRefs) {
			if(fr.label.equals(label)) {
				Code.fixup(fr.location);
			}
		}
		
	}
	
	public void visit(GotoStatement gotoStatement) {
		
		if(labelLocations.containsKey(gotoStatement.getLabel())) {
			Code.putJump(labelLocations.get(gotoStatement.getLabel()));
		} else {
			Code.putJump(0);
			forwardRefs.add(new ForwardReference(gotoStatement.getLabel(), Code.pc - 2));
		}
		
	}
	
	public void visit(ComplexExpr complexExpr) {
		
		if(complexExpr.getAddop() instanceof AddopL) {
			
			AddopL addop = (AddopL)complexExpr.getAddop();
			if(addop.getAddopLeft() instanceof Add) { Code.put(Code.add); }
			else { Code.put(Code.sub); }
			
		} else { // AddopR
			
			AddopR addop = (AddopR)complexExpr.getAddop();
			if(addop.getAddopRight() instanceof AddEq) { Code.put(Code.add); }
			else { Code.put(Code.sub); }
			
			SimpleExpr se = (SimpleExpr)complexExpr.getExpr();
			SingleFactor sf = (SingleFactor)se.getTerm();
			DesignatorFactor df = (DesignatorFactor)sf.getFactor();
			Designator d = (Designator)df.getDesignator();
			
			if(d instanceof DesignatorArrayEl) {

				Code.put(Code.dup_x2);
				
				if(d.obj.getType().getElemType() == SymTab.charType) {
					Code.put(Code.bastore);
				} else {
					Code.put(Code.astore);
				}
				
			} else { // DesignatorScalar
				
				Code.put(Code.dup);
				
				Code.store(d.obj);
			}
			
		}
		
	}
	
	public void visit(SubsetOfExpr subsetOfExpr) {
		
		Code.load(subsetOfExpr.getSubsetOperand().obj);
		Code.load(subsetOfExpr.getSubsetOperand1().obj);
		Code.put(Code.enter);
		Code.put(2);
		Code.put(5);
		
		// if(indA < len(a))
		int loop1Adr = Code.pc;
		Code.put(Code.load_n + 2);
		Code.put(Code.load_n + 0);
		Code.put(Code.arraylength);
		Code.putFalseJump(Code.lt, 0);
		int fixup1 = Code.pc - 2;
		
		// indB = 0, found = 0
		Code.loadConst(0);
		Code.put(Code.store);
		Code.put(4);
		Code.loadConst(0);
		Code.put(Code.store_n + 3);
		
		// if(indB < len(b))
		int loop2Adr = Code.pc;
		Code.put(Code.load_n + 3);
		Code.put(Code.load_n + 1);
		Code.put(Code.arraylength);
		Code.putFalseJump(Code.lt, 0);
		int fixup2 = Code.pc - 2;
		
		// if(a[indA] == b[indB])
		Code.put(Code.load_n + 0);
		Code.put(Code.load_n + 2);
		Code.put(Code.aload);
		Code.put(Code.load_n + 1);
		Code.put(Code.load_n + 3);
		Code.put(Code.aload);
		Code.putFalseJump(Code.eq, 0);
		int fixup3 = Code.pc - 2;
		
		// found = 1
		Code.loadConst(1);
		Code.put(Code.store);
		Code.put(4);
		Code.putJump(0);
		int fixup4 = Code.pc - 2;
		
		// indB++; jmp loop2;
		Code.fixup(fixup3);
		Code.put(Code.load_n + 3);
		Code.loadConst(1);
		Code.put(Code.add);
		Code.put(Code.store_n + 3);
		Code.putJump(loop2Adr);
		
		// if(found == 0)
		Code.fixup(fixup2);
		Code.fixup(fixup4);
		Code.put(Code.load);
		Code.put(4);
		Code.loadConst(1);
		Code.putFalseJump(Code.eq, 0);
		int fixup5 = Code.pc - 2;
		
		// indA++; jmp loop1;
		Code.put(Code.load_n + 2);
		Code.loadConst(1);
		Code.put(Code.add);
		Code.put(Code.store_n + 2);
		Code.putJump(loop1Adr);
		
		// return false;
		Code.fixup(fixup5);
		Code.loadConst(0);
		Code.putJump(0);
		int fixup6 = Code.pc - 2;
		
		// return true;
		Code.fixup(fixup1);
		Code.loadConst(1);
		
		Code.fixup(fixup6);
		Code.put(Code.exit);
		
	}
	
	public void visit(NegatedFactor negatedFactor) {
		Code.put(Code.neg);
	}
	
	public void visit(MultipleFactors multipleFactors) {
		
		if(multipleFactors.getMulop() instanceof MulopL) {
			
			MulopL mulop = (MulopL)multipleFactors.getMulop(); 
			if(mulop.getMulopLeft() instanceof Mul) { Code.put(Code.mul); }
			else if(mulop.getMulopLeft() instanceof Div) { Code.put(Code.div); }
			else { Code.put(Code.rem); }
			
		} else { // MulopR
			
			MulopR mulop = (MulopR)multipleFactors.getMulop();
			if(mulop.getMulopRight() instanceof MulEq) { Code.put(Code.mul); }
			else if(mulop.getMulopRight() instanceof DivEq) { Code.put(Code.div); }
			else { Code.put(Code.rem); }
			
			SingleFactor sf = (SingleFactor)multipleFactors.getTerm();
			DesignatorFactor df = (DesignatorFactor)sf.getFactor();
			Designator d = (Designator)df.getDesignator();
			
			if(d instanceof DesignatorArrayEl) {

				Code.put(Code.dup_x2);
				
				if(d.obj.getType().getElemType() == SymTab.charType) {
					Code.put(Code.bastore);
				} else {
					Code.put(Code.astore);
				}
				
			} else { // DesignatorScalar
				
				Code.put(Code.dup);
				
				Code.store(d.obj);
			}
			
		}
		
	}
	
	public void visit(DesignatorFactor designatorFactor) {
		
if(designatorFactor.getDesignator() instanceof DesignatorArrayEl) {
			
			// MOZDA OVDE RADITI DUPLIRANJE ADRESE I INDEKSA NIZA
			// AKO JE LEVO OD KOMB. OP.		
			
			if(designatorFactor.getParent() instanceof SingleFactor) {
				
				SingleFactor sf = (SingleFactor)designatorFactor.getParent();
				if(sf.getParent() instanceof ComplexExpr) {
					
					// Desna strana izraza pa ne radimo nista
					
				} else if(sf.getParent() instanceof SimpleExpr) { // SimpleExpr
					
					SimpleExpr se = (SimpleExpr)sf.getParent();
					if(se.getParent() instanceof ComplexExpr) {
						Code.put(Code.dup2);
					}
					
				} else { // MultipleFactors
					
					MultipleFactors mf = (MultipleFactors)sf.getParent();
					if(mf.getMulop() instanceof MulopR) {
						Code.put(Code.dup2);
					}
					
				}
				
			} else { // MultipleFactors
				// Faktor je ali sa desne strane operatora pa ne radimo nista
			}
			
//			else if(parent.getParent() instanceof AssignmentStatement) {
//				
//				AssignmentStatement assignStmt = (AssignmentStatement)parent.getParent();
//				if(assignStmt.getAssignop() instanceof AssignAdd) {
//					Code.put(Code.dup2);
//				}
//				else if(assignStmt.getAssignop() instanceof AssignMul) {
//					Code.put(Code.dup2);
//				}
//				
//			}
			
			DesignatorArrayEl d = (DesignatorArrayEl)designatorFactor.getDesignator(); 
			if(d.obj.getType().getElemType() == SymTab.charType) {
				Code.put(Code.baload);
			} else {
				Code.put(Code.aload);
			}
			
		} else { // DesignatorScalar
			
			DesignatorScalar d = (DesignatorScalar)designatorFactor.getDesignator();
			Code.load(d.obj);
			
		}
		
	}
	
	public void visit(ConstFactor constFactor) {
		
		Obj con = Tab.insert(Obj.Con, "$", constFactor.struct);
		con.setLevel(0);
		if(constFactor.struct == SymTab.intType) {
			con.setAdr(((NumConst)constFactor.getConstValue()).getVal().intValue());
		} else if(constFactor.struct == SymTab.charType) {
			con.setAdr(((CharConst)constFactor.getConstValue()).getVal().charValue());
		} else { // boolType
			con.setAdr(((BoolConst)constFactor.getConstValue()).getVal().intValue());
		}
		
		Code.load(con);
		
	}
	
	public void visit(NewArrayFactor newArrayFactor) {
		
		Code.put(Code.newarray);
		if(newArrayFactor.getType().struct == SymTab.charType) {
			Code.put(0);
		} else {
			Code.put(1);
		}
		
	}
	
	public void visit(FuncCallFactor funcCallFactor) {
		Obj functionObj = funcCallFactor.getDesignator().obj;
		int offset = functionObj.getAdr() - Code.pc;
		Code.put(Code.call);
		Code.put2(offset);
		
	}
	
	public void visit(DesignatorArrayEl designatorArrayEl) {
		if(designatorArrayEl.getParent() instanceof AssignmentStatement) {
			
			AssignmentStatement as = (AssignmentStatement)designatorArrayEl.getParent();
			if(!(as.getAssignop() instanceof AssignBasic)) {
				Code.put(Code.dup2);
				Code.put(Code.aload);
			}
			
		}
	}
	
	public void visit(DesignatorScalar designatorScalar) {
		
		if(designatorScalar.getParent() instanceof AssignmentStatement) {
			
			AssignmentStatement as = (AssignmentStatement)designatorScalar.getParent();
			if(!(as.getAssignop() instanceof AssignBasic)) {
				Code.load(designatorScalar.obj);
			}
			
		}
		
	}
	
	public void visit(ArrayDesignatorId arrayDesignatorId) {
		if(arrayDesignatorId.getParent() instanceof DesignatorArrayEl) {
			DesignatorArrayEl parent = (DesignatorArrayEl)arrayDesignatorId.getParent();
			Code.load(parent.obj);
		}
	}
	
}
