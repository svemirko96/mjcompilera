package rs.ac.bg.etf.pp1;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.CounterVisitor.ActualParamCounter;
import rs.ac.bg.etf.pp1.CounterVisitor.FormalParamCounter;
import rs.ac.bg.etf.pp1.SymTab;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class SemanticAnalyzer extends VisitorAdaptor {

	Logger log = Logger.getLogger(getClass());
	
	// kontekst semantickog analizatora
	Obj currentMethod = null;
	Type currentType = null;
	int curArgument = 0;
	boolean returnFound = false;
	boolean errorDetected = false;
	int nVars;
	
	public void report_error(String message, SyntaxNode info) {
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if(line != 0)
			msg.append("na liniji ").append(line);
		log.error(msg.toString());
	}
	
	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if(line != 0)
			msg.append(" na liniji ").append(line);
		log.info(msg.toString());
	}

	public boolean passed() {
		return !errorDetected;
	}
	
	// ------------- semanticke provere ---------------------
	
	// ----- Opsti kontekstni uslovi -----
	
	// - Svako ime u programu mora biti deklarisano pre prvog koriscenja
	// - Ime ne sme biti deklarisano vise puta unutar istog opsega
	// - (X) U programu mora postojati metoda sa imenom main. Ona mora biti deklarisana kao void metoda bez argumenata
	
	// ----- Kontekstni uslovi za standardne metode -----
	// - char(e) e mora biti izraz tipa int
	// - ordr(c) c mora biti tipa char
	// - len(a) a mora biti niz ili znakovni niz
	
	// Program ::= PROGRAM ProgName ProgMemberList LBRACE MethodsDeclList RBRACE
	public void visit(Program program) {
		nVars = SymTab.currentScope.getnVars();
		
		Obj mainFunc = SymTab.find("main");
		if(mainFunc == SymTab.noObj) {
			report_error("Linija " + program.getLine() + ": Obavezno je definisati glavnu funkciju void main()", null);
		}
			
		SymTab.chainLocalSymbols(program.getProgName().obj);
		SymTab.closeScope();
	}
	
	// ProgName ::= IDENT:progId;
	public void visit(ProgName progName) {
		
		if(progName.getProgId().equals("main")) {
			report_error("Linija " + progName.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		if(SymTab.find(progName.getProgId()) != SymTab.noObj) {
			report_error("Linija " + progName.getLine() + ": Ime " + progName.getProgId() + " je vec deklarisano.", null);
		}
		
		progName.obj = SymTab.insert(Obj.Prog, progName.getProgId(), SymTab.noType);
		SymTab.openScope();
	}
	
	// ConstDecl ::= CONST ConstDeclType ConstAssignList SEMICOL
	public void visit(ConstDecl constDecl) {
		currentType = null;
	}
	
	// ConstDeclType ::= Type:type
	// - type mora predstavljati samo proste tipove (int, char, bool)
	public void visit(ConstDeclType constDeclType) {
		
		currentType = constDeclType.getType();
		
		if(!(currentType.getTypeId().equals("int") || currentType.getTypeId().equals("char") || currentType.getTypeId().equals("bool"))) {
			report_error("Linija " + constDeclType.getLine() + ": Konstante mogu biti samo prostog tipa", null);
		}
		
	}
	
	// ConstAssignment ::= IDENT:constId ASSIGN ConstValue
	// - tip terminala numConst, charConst, boolConst mora biti EKVIVALENTAN tipu Type
	// - ime constId ne sme biti prethodno deklarisano
	public void visit(ConstAssignment constAssignment) {
		
		if(!constAssignment.getConstValue().struct.equals(currentType.struct)) {
			report_error("Linija " + constAssignment.getLine() + ": Tipovi pri deklaraciji konstante nisu ekvivalentni", null);
		}
		
		if(constAssignment.getConstId().equals("main")) {
			report_error("Linija " + constAssignment.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		else if(SymTab.find(constAssignment.getConstId()) != SymTab.noObj) { 
			report_error("Linija " + constAssignment.getLine() + ": Ime " + constAssignment.getConstId() + " je vec deklarisano", null);
		}
		else {
			constAssignment.obj = SymTab.insert(Obj.Con, constAssignment.getConstId(), currentType.struct);
			
			if(currentType.struct == SymTab.intType) {
				constAssignment.obj.setAdr(((NumConst)constAssignment.getConstValue()).getVal().intValue());
			} else if(currentType.struct == SymTab.charType) {
				constAssignment.obj.setAdr(((CharConst)constAssignment.getConstValue()).getVal().charValue());
			} else { // boolType
				constAssignment.obj.setAdr(((BoolConst)constAssignment.getConstValue()).getVal().intValue());
			}
		}
		
	}
	
	// VarDeclType ::= Type:type
	public void visit(VarDeclType varDeclType) {
		currentType = varDeclType.getType();
	}
	
	// SingleVarDecl ::= IDENT:varId LBRACKET RBRACKET
	public void visit(VarDeclArray varDeclArray) {
		
		if(varDeclArray.getVarId().equals("main")) {
			report_error("Linija " + varDeclArray.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		else if(SymTab.find(varDeclArray.getVarId()) != SymTab.noObj) {
			report_error("Linija " + varDeclArray.getLine() + ": Ime " + varDeclArray.getVarId() + " je vec deklarisano", null);
		}
		else {
			varDeclArray.obj = SymTab.insert(Obj.Var, varDeclArray.getVarId(), new Struct(Struct.Array, currentType.struct));
		}
		
		//log.info("Uspesno deklarisan niz " + varDeclArray.getVarId());
	}
	
	// SingleVarDecl ::= IDENT:varId
	// - ime ne sme biti prethodno deklarisano
	public void visit(VarDeclScalar varDeclScalar) {
		
		if(varDeclScalar.getVarId().equals("main")) {
			report_error("Linija " + varDeclScalar.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		else if(SymTab.find(varDeclScalar.getVarId()) != SymTab.noObj) {
			report_error("Linija " + varDeclScalar.getLine() + ": Ime " + varDeclScalar.getVarId() + " je vec deklarisano", null);
		}
		else {
			varDeclScalar.obj = SymTab.insert(Obj.Var, varDeclScalar.getVarId(), currentType.struct);
		}
		
	}
	
	// MethodDecl ::= MethodTypeName LPAREN RPAREN VarDecls LBRACE StatementList RBRACE
	public void visit(MethodDecl methodDecl) {
		
		if(!returnFound && currentMethod.getType() != SymTab.noType) {
			report_error("Semanticka greska na liniji " + methodDecl.getLine() + ": funkcija " + currentMethod.getName() + " nema return iskaz", null);
		}
		
		SymTab.chainLocalSymbols(currentMethod);
		SymTab.closeScope();
	
		returnFound = false;
		currentMethod = null;
	}
	
	// MethodTypeName ::= VOID IDENT:methodId
	// - ime ne sme biti prethodno deklarisano
	public void visit(MethodTypeName methodTypeName) {
		
		if(SymTab.find(methodTypeName.getMethodId()) != SymTab.noObj) {
			report_error("Linija " + methodTypeName.getLine() + ": Ime " + methodTypeName.getMethodId() + " je vec deklarisano", null);
		}
		else if(methodTypeName.getMethodId().equals("main") && !(methodTypeName.getReturnType() instanceof ReturnTypeVoid)) {
			report_error("Linija " + methodTypeName.getLine() + ": Glavna funkcija main mora biti tipa void", null);
		}
			
		currentMethod = methodTypeName.obj = SymTab.insert(Obj.Meth, methodTypeName.getMethodId(), methodTypeName.getReturnType().struct);
		
		SyntaxNode methodNode = methodTypeName.getParent();
		FormalParamCounter fpc = new FormalParamCounter();
		methodNode.traverseTopDown(fpc);
		currentMethod.setLevel(fpc.getCount());
		
		
		SymTab.openScope();
		
	}
	
	// ReturnType ::= Type:type
	public void visit(ReturnTypeScalar returnTypeScalar) {
		returnTypeScalar.struct = returnTypeScalar.getType().struct;
		returnFound = true;
	}
	
	// ReturnType ::= VOID
	public void visit(ReturnTypeVoid returnTypeVoid) {
		returnTypeVoid.struct = SymTab.noType;
	}
	
	// FormalParamDecl ::= Type IDENT:name
	public void visit(FormalParamScalar formalParamScalar) {
		
		if(currentMethod.getName().equals("main")) {
			report_error("Linija " + formalParamScalar.getLine() + ": main metoda ne moze imati parametre", null);
		}
		
		if(formalParamScalar.getName().equals("main")) {
			report_error("Linija " + formalParamScalar.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		else if(SymTab.find(formalParamScalar.getName()) != SymTab.noObj) {
			report_error("Linija " + formalParamScalar.getLine() + ": Ime " + formalParamScalar.getName() + " je vec deklarisano", null);
		}
		else {
			SymTab.insert(Obj.Var, formalParamScalar.getName(), formalParamScalar.getType().struct);
		}
		
	}
	
	// FormalParamDecl ::= Type IDENT:name LBRACKET RBRACKET
	public void visit(FormalParamArray formalParamArray) {
		
		if(currentMethod.getName().equals("main")) {
			report_error("Linija " + formalParamArray.getLine() + ": main metoda ne moze imati parametre", null);
		}
		
		if(formalParamArray.getName().equals("main")) {
			report_error("Linija " + formalParamArray.getLine() + ": Ime main je rezervisano za glavnu funkciju programa", null);
		}
		if(SymTab.find(formalParamArray.getName()) != SymTab.noObj) {
			report_error("Linija " + formalParamArray.getLine() + ": Ime " + formalParamArray.getName() + " je vec deklarisano", null);
		}
		else {
			SymTab.insert(Obj.Var, formalParamArray.getName(), new Struct(Struct.Array, formalParamArray.getType().struct));
		}
		
	}
	
	// Type ::= IDENT:typeId
	// - typeId mora oznacavati tip podataka
	public void visit(Type type) {
		Obj typeObj = SymTab.find(type.getTypeId());
		
		if(typeObj == SymTab.noObj) {
			report_error("Linija " + type.getLine() + ": Tip " + type.getTypeId() + " nije definisan", null);
			type.struct = Tab.noType;
		}
		else if(typeObj.getKind() != Obj.Type) {
			report_error("Linija " + type.getLine() + ": " + type.getTypeId() + " ne predstavlja tip", null);
			type.struct = Tab.noType;
		}
		else {
			type.struct = typeObj.getType();
		}
	}
	
	// ConstValue ::= NUM_CONST:val
	public void visit(NumConst numConst) {
		numConst.struct = SymTab.intType;
	}
	
	// ConstValue ::= CHAR_CONST:val
	public void visit(CharConst charConst) {
		charConst.struct = SymTab.charType;
	}

	// ConstValue ::= BOOL_CONST:val
	public void visit(BoolConst boolConst) {
		boolConst.struct = SymTab.boolType;
	}
	
	// Statement ::= RETURN SEMICOL
	// - povratni tip funkcije mora biti void
	public void visit(ReturnStatement returnStatement) {
		
		if(currentMethod.getType() != SymTab.noType) {
			report_error("Linija " + returnStatement.getLine() + ": Return iskaz mora vratiti vrednost odgovarajuceg tipa", null);
		}
		
	}

	// Statement ::= RETURN Expr SEMICOL
	// - tip neterminala Expr mora biti ekvivalentan povratnom tipu funkcije 
	public void visit(ReturnExprStatement returnExprStatement) {
		
		if(currentMethod.getType() == SymTab.noType) {
			report_error("Linija " + returnExprStatement.getLine() + ": Nedozvoljeno vracanje vrednosti u funkciji tipa void", null);
		}
		else if(!returnExprStatement.getExpr().struct.equals(currentMethod.getType())) {
			report_error("Linija " + returnExprStatement.getLine() + ": Tip vracene vrednosti nije ekvivalentan povratnom tipu funkcije", null);
		}
		
	}
	
	// Statement ::= READ LPAREN Designator RPAREN SEMICOL
	public void visit(ReadStatement readStatement) {
		
		if(readStatement.getDesignator() instanceof DesignatorScalar) {
			
			Obj designatorObj = readStatement.getDesignator().obj;
			if(designatorObj.getKind() != Obj.Var || designatorObj.getKind() == Obj.Var && designatorObj.getType().getKind() == Struct.Array) {
				report_error("Linija " + readStatement.getLine() + ": Funkciji read mogu biti prosledjeni samo promenljiva ili element niza", null);
			}
			else if(designatorObj.getType() != SymTab.intType && designatorObj.getType() != SymTab.charType && designatorObj.getType() != SymTab.boolType) {
				report_error("Linija " + readStatement.getLine() + ": Funkcija read moze ucitati samo tipove int, char i bool", null);
			}
			
		} else { // DesignatorArrayEl
			
			Struct elemType = readStatement.getDesignator().obj.getType().getElemType();
			if(elemType != SymTab.intType && elemType != SymTab.charType && elemType != SymTab.boolType) {
				report_error("Linija " + readStatement.getLine() + ": Funkcija read moze ucitati samo tipove int, char i bool", null);
			}
			
		}
		
	}
	
	// Statement ::= PRINT LPAREN Expr RPAREN SEMICOL
	public void visit(PrintNoWidthStatement printNoWidthStatement) {
		
		Struct exprStruct = printNoWidthStatement.getExpr().struct;
		if(exprStruct != SymTab.intType && exprStruct != SymTab.charType && exprStruct != SymTab.boolType) {
			report_error("Linija " + printNoWidthStatement.getLine() + ": Izraz prosledjen print funkciji mora biti tipa int, char ili bool", null);
		}
		
	}
	
	// Statement ::= PRINT LPAREN Expr COMMA NUM_CONST RPAREN SEMICOL
	public void visit(PrintSpecWidthStatement printspecWidthStatement) {
		
		Struct exprStruct = printspecWidthStatement.getExpr().struct;
		if(exprStruct != SymTab.intType && exprStruct != SymTab.charType && exprStruct != SymTab.boolType) {
			report_error("Linija " + printspecWidthStatement.getLine() + ": Izraz prosledjen print funkciji mora biti tipa int, char ili bool", null);
		}
		
	}
	
	// Statement ::= Designator LPAREN ActualParams RPAREN SEMICOL
	// - Broj formalnih i stvarnih argumenata funkcije mora biti isti
	// - Tip svakog stvarnog argumenta mora biti kompatibilan pri dodelisa tipom svakog formalnog argumenta na odgovarajucoj poziciji
	public void visit(FuncCallStatement funcCallStatement) {
		
		Obj funcObj = funcCallStatement.getDesignator().obj;
		if(funcObj.getKind() != Obj.Meth) {
			report_error("Linija " + funcCallStatement.getLine() + ": " + funcObj.getName() + " nije ime funkcije", null);
		}
		
		ActualParamCounter apc = new ActualParamCounter();
		funcCallStatement.traverseTopDown(apc);
		if(funcObj.getLevel() != apc.getCount()) {
			report_error("Linija " + funcCallStatement.getLine() + ": Broj argumenata prosledjenih funkciji nije validan", null);
		}
		
		curArgument = 0;
	}
	
	// ActualParamList ::= ActualParamList COMMA Expr
	// - Tip svakog stvarnog argumenta mora biti kompatibilan pri dodelisa tipom svakog formalnog argumenta na odgovarajucoj poziciji
	public void visit(MultipleActualParams multipleActualParams) {
		
		SyntaxNode funcCallNode = multipleActualParams;
		while(!(funcCallNode instanceof FuncCallStatement) && !(funcCallNode instanceof FuncCallFactor))
			funcCallNode = funcCallNode.getParent();
		
		Obj funcObj;
		if(funcCallNode instanceof FuncCallStatement)
			funcObj = ((FuncCallStatement)funcCallNode).getDesignator().obj;
		else
			funcObj = ((FuncCallFactor)funcCallNode).getDesignator().obj;
		
		if(!multipleActualParams.getExpr().struct.assignableTo(getNthParameter(funcObj, curArgument).getType())) {
			report_error("Linija " + multipleActualParams.getLine() + ": Neispravan tip " + (curArgument + 1) + ". prosledjenog argumenta", null);
		}
		
		curArgument++;
	}
	
	// SingleActualParam ::= Expr
	// - Tip svakog stvarnog argumenta mora biti kompatibilan pri dodelisa tipom svakog formalnog argumenta na odgovarajucoj poziciji
	public void visit(SingleActualParam singleActualParam) {
		
		SyntaxNode funcCallNode = singleActualParam;
		while(!(funcCallNode instanceof FuncCallStatement) && !(funcCallNode instanceof FuncCallFactor))
			funcCallNode = funcCallNode.getParent();
		
		Obj funcObj;
		if(funcCallNode instanceof FuncCallStatement)
			funcObj = ((FuncCallStatement)funcCallNode).getDesignator().obj;
		else
			funcObj = ((FuncCallFactor)funcCallNode).getDesignator().obj;
		
		if(!singleActualParam.getExpr().struct.assignableTo(getNthParameter(funcObj, curArgument).getType())) {
			report_error("Linija " + singleActualParam.getLine() + ": Neispravan tip " + (curArgument + 1) + ". prosledjenog argumenta", null);
		}
		
		curArgument++;
	}
	
	// DesignatorStatement ::= Designator Assignop Expr
	// - Designator mora oznacavati promenljivu ili element niza
	// - tip terminala Expr mora biti KOMPATIBILAN PRI DODELI sa tipom Designator
	public void visit(AssignmentStatement assignmentStatement) {
		
		// provera uslova da leva strana dodele predstavlja promenljivu ili element niza
		Designator d = assignmentStatement.getDesignator();
		if(d.obj.getKind() != Obj.Var) {
			String varId = d instanceof DesignatorArrayEl ? ((DesignatorArrayEl)d).getArrayDesignatorId().getVarId() : ((DesignatorScalar)d).getVarId();
			report_error("Linija " + assignmentStatement.getLine() + ": " + varId + " nije validna promenljiva ili niz za upis vrednosti", null);
		}
		
		// provera uslova da tip desne strane mora biti kompatibilan pri dodeli sa tipom leve strane
		if(d instanceof DesignatorArrayEl) {
			if(!assignmentStatement.getExpr().struct.assignableTo(d.obj.getType().getElemType())) {
				report_error("Linija " + assignmentStatement.getLine() + ": Nekompatibilnost tipova pri dodeli vrednosti", null);
			}
		}
		else {
			if(!assignmentStatement.getExpr().struct.assignableTo(d.obj.getType())) {
				report_error("Linija " + assignmentStatement.getLine() + ": Nekompatibilnost tipova pri dodeli vrednosti", null);
			}
		}
		
	}
	
	// DesignatorStatement ::= Designator INC
	// - Designator mora oznacavati promenljivu ili element niza
	// - Designator mora biti tipa int
	public void visit(IncrementStatement incrementStatement) {
		
		// provera uslova da leva strana dodele predstavlja promenljivu ili element niza
		Designator d = incrementStatement.getDesignator();
		if(d.obj.getKind() != Obj.Var) {
			String varId = d instanceof DesignatorArrayEl ? ((DesignatorArrayEl)d).getArrayDesignatorId().getVarId() : ((DesignatorScalar)d).getVarId();
			report_error("Linija " + incrementStatement.getLine() + ": " + varId + " nije validna promenljiva ili niz za upis vrednosti", null);
		}
		
		// provera uslova da tip desne strane mora biti kompatibilan pri dodeli sa tipom leve strane
		if(d instanceof DesignatorArrayEl) {
			if(d.obj.getType().getElemType() != SymTab.intType) {
				report_error("Linija " + incrementStatement.getLine() + ": Tip element niza koji se inkrementira mora biti int", null);
			}
		}
		else {
			if(d.obj.getType() != SymTab.intType) {
				report_error("Linija " + incrementStatement.getLine() + ": Tip promenljive koja se inkrementira mora biti int", null);
			}
		}
		
	}
	
	// DesignatorStatement ::= Designator DEC
	// - Designator mora oznacavati promenljivu ili element niza
	// - Designator mora biti tipa int
	public void visit(DecrementStatement decrementStatement) {
		
		// provera uslova da leva strana dodele predstavlja promenljivu ili element niza
		Designator d = decrementStatement.getDesignator();
		if(d.obj.getKind() != Obj.Var) {
			String varId = d instanceof DesignatorArrayEl ? ((DesignatorArrayEl)d).getArrayDesignatorId().getVarId() : ((DesignatorScalar)d).getVarId();
			report_error("Linija " + decrementStatement.getLine() + ": " + varId + " nije validna promenljiva ili niz za upis vrednosti", null);
		}
		
		// provera uslova da tip desne strane mora biti kompatibilan pri dodeli sa tipom leve strane
		if(d instanceof DesignatorArrayEl) {
			if(d.obj.getType().getElemType() != SymTab.intType) {
				report_error("Linija " + decrementStatement.getLine() + ": Tip element niza koji se dekrementira mora biti int", null);
			}
		}
		else {
			if(d.obj.getType() != SymTab.intType) {
				report_error("Linija " + decrementStatement.getLine() + ": Tip promenljive koja se dekrementira mora biti int", null);
			}
		}
		
	}
	
	// Expr ::= Expr Addop Term
	// - Expr i ExprPart moraju biti tipa int. U svakom slucaju, tipovi za Expr i ExprPart moraju biti KOMPATIBILNI
	public void visit(ComplexExpr complexExpr) {
		
		if(complexExpr.getExpr().struct != SymTab.intType || complexExpr.getTerm().struct != SymTab.intType) {
			report_error("Linija " + complexExpr.getLine() + ": Operacija mnozenja je definisana samo za int tip", null);
		}
		
		if(!complexExpr.getExpr().struct.compatibleWith(complexExpr.getTerm().struct)) {
			report_error("Linija " + complexExpr.getLine() + ": Pri operaciji sabiranja tipovi nisu kompatibilni", null);
		}
		
		complexExpr.struct = SymTab.intType;
		
	}
	
	// Expr ::= Term
	public void visit(SimpleExpr simpleExpr) {
		simpleExpr.struct = simpleExpr.getTerm().struct;
	}
	
	public void visit(SubsetOfExpr subsetOfExpr) {
		subsetOfExpr.struct = SymTab.boolType;
	}
	
	public void visit(SubsetOperand subsetOperand) {
		subsetOperand.obj = SymTab.find(subsetOperand.getArrayDesignatorId().getVarId());
	}
	
	// Term ::= Term Mulop Factor
	// - Term i Factor moraju biti tipa int
	public void visit(MultipleFactors multipleFactors) {
		
		if(multipleFactors.getTerm().struct != SymTab.intType || multipleFactors.getFactor().struct != SymTab.intType) {
			report_error("Linija " + multipleFactors.getLine() + ": Operacija mnozenja je definisana samo za int tip", null);
		}
		
		multipleFactors.struct = SymTab.intType;
		
	}
	
	// Term ::= Factor
	public void visit(SingleFactor singleFactor) {
		singleFactor.struct = singleFactor.getFactor().struct;
	}
	
	// Factor ::= Designator
	public void visit(DesignatorFactor designatorFactor) {
		if(designatorFactor.getDesignator() instanceof DesignatorScalar) {
			DesignatorScalar d = (DesignatorScalar)designatorFactor.getDesignator();
			if(d.obj.getKind() != Obj.Var && d.obj.getKind() != Obj.Con) {
				report_error("Linija " + designatorFactor.getLine() + ": " + d.getVarId() + " mora biti promenljiva ili konstanta", null);
			}
			designatorFactor.struct = designatorFactor.getDesignator().obj.getType();
		}
		else { // DesignatorArrayEl
			designatorFactor.struct = designatorFactor.getDesignator().obj.getType().getElemType();
		}
	}
	
	// Factor ::= ConstValue
	public void visit(ConstFactor constFactor) {
		constFactor.struct = constFactor.getConstValue().struct;
	}
	
	// Factor ::= LPAREN Expr RPAREN
	public void visit(GroupedExprFactor groupedExprFactor) {
		groupedExprFactor.struct = groupedExprFactor.getExpr().struct;
	}
	
	// Factor ::= NEW Type LBRACKET Expr RBRACKET
	// - Tip neterminala Expr mora biti int
	public void visit(NewArrayFactor newArrayFactor) {
		
		if(newArrayFactor.getExpr().struct != SymTab.intType) {
			report_error("Linija " + newArrayFactor.getLine() + ": Izraz u zagradama mora biti tipa int", null);
		}
		
		newArrayFactor.struct = new Struct(Struct.Array, newArrayFactor.getType().struct);
		
	}
	
	// Factor ::= MINUS Factor
	// - Factor mora biti tipa int
	public void visit(NegatedFactor negatedFactor) {
		
		if(negatedFactor.getFactor().struct != SymTab.intType) {
			report_error("Linija " + negatedFactor.getLine() + ": Operacija negacije je definisana samo za int tip", null);
		}
		
		negatedFactor.struct = SymTab.intType;
		
	}
	
	// Factor ::= Designator LPAREN ActualParams RPAREN SEMICOL
	// - Broj formalnih i stvarnih argumenata funkcije mora biti isti
	// - Tip svakog stvarnog argumenta mora biti kompatibilan pri dodelisa tipom svakog formalnog argumenta na odgovarajucoj poziciji
	public void visit(FuncCallFactor funcCallFactor) {
		
		Obj funcObj = funcCallFactor.getDesignator().obj;
		if(funcObj.getKind() != Obj.Meth) {
			report_error("Linija " + funcCallFactor.getLine() + ": " + funcObj.getName() + " nije ime funkcije", null);
		}
		
		ActualParamCounter apc = new ActualParamCounter();
		funcCallFactor.traverseTopDown(apc);
		if(funcObj.getLevel() != apc.getCount()) {
			report_error("Linija " + funcCallFactor.getLine() + ": Broj argumenata prosledjenih funkciji nije validan", null);
		}
		
		funcCallFactor.struct = funcCallFactor.getDesignator().obj.getType();
		
		curArgument = 0;
	}
	
	// Designator ::= IDENT:varId LBRACKET Expr RBRACKET
	// - tip varId mora biti niz
	// - tip Expr mora biti int
	public void visit(DesignatorArrayEl designatorArrayEl) {
		
		// provera tipa leve strane - mora biti niz
		Obj obj = SymTab.find(designatorArrayEl.getArrayDesignatorId().getVarId());
		if(obj == SymTab.noObj) {
			report_error("Linija " + designatorArrayEl.getLine() + ": Ime " + designatorArrayEl.getArrayDesignatorId().getVarId() + " nije deklarisano", null);
			designatorArrayEl.obj = SymTab.noObj;
		}
		else if(obj.getKind() != Obj.Var || obj.getType().getKind() != Struct.Array) {
			report_error("Linija " + designatorArrayEl.getLine() + ": " + designatorArrayEl.getArrayDesignatorId().getVarId() + " nije tipa niz", null);
			designatorArrayEl.obj = SymTab.noObj;
		}
		else {
			designatorArrayEl.obj = obj;
		}
		
		// provera tipa expr - mora biti int
		if(!designatorArrayEl.getExpr().struct.equals(SymTab.intType)) {
			report_error("Linija " + designatorArrayEl.getLine() + ": Izraz prilikom indeksiranja niza mora biti tipa int", null);
		}
		
	}
	
	// Designator ::= IDENT:varId
	// - varId mora biti ime promenljive
	public void visit(DesignatorScalar designatorScalar) {
		
		Obj obj = SymTab.find(designatorScalar.getVarId());
		if(obj == SymTab.noObj) {
			report_error("Linija " + designatorScalar.getLine() + ": Ime " + designatorScalar.getVarId() + " nije deklarisano", null);
			designatorScalar.obj = SymTab.noObj;
		}
		else {
			designatorScalar.obj = obj;
		}
		
	}
	
	// -------------------- pomocne metode ----------------------
	
	public Obj getNthParameter(Obj funcObj, int n) {
		
		if(funcObj.getLevel() < n + 1) {
			return SymTab.noObj;
		}
		
		Collection<Obj> locals = funcObj.getLocalSymbols();
		Iterator<Obj> it = locals.iterator();
		for(int i = 0; i < n; i++) {
			it.next();
		}
		
		return it.next();
	}
	
}
